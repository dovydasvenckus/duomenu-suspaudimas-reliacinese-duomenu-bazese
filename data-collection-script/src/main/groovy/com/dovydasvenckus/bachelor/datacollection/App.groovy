package com.dovydasvenckus.bachelor.datacollection

import java.text.DecimalFormat

class App {
    static DecimalFormat df = new DecimalFormat("###.###")
    static String tableFormat = "%15s%15s%15s\n"


    static void main(String[] args) {
        Integer startCount = args[0] as Integer
        String dataColumnName = args[1]

        List<String> valueStrings = args.takeRight(args.length - 2)
        List<Double> values = valueStrings.collect {
            Double.parseDouble(it)
        }

        printData(values, startCount, dataColumnName)
    }

    static printData(List<Double> values, Integer startCount, String dataColumnName) {
        System.out.format(tableFormat, 'number', dataColumnName, 'count')

        values.eachWithIndex { currentValue, i ->
            Double count = startCount * Math.pow(10, i)
            System.out.format(tableFormat, i, df.format(currentValue), df.format(count))
        }
    }
}