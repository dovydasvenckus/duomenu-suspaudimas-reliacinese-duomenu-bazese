#!/usr/bin/env bash
SQL_FILE=${1?Give valid file name}
OUT_FILE=${SQL_FILE#"../"}
sed '1 i\SET NOCOUNT ON;\nBEGIN TRANSACTION\n' "${SQL_FILE}" | sed '0~500 s/$/\nGO/g' | sed '$s/$/\n\nCOMMIT TRANSACTION\nGO/' > "transactional_$OUT_FILE"