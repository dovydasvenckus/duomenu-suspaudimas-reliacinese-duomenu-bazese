#!/usr/bin/env bash
SQL_FILE=${1?Give valid file name}
sqlcmd -S localhost -d playground_columnstore -i ../../../sql/MsSql/text_short/text_short_columnstore_compression.sql
time sqlcmd -S localhost -d playground_columnstore -i $SQL_FILE
