#!/usr/bin/env bash
SQL_FILE=${1?Give valid file name}
sqlcmd -S localhost -d playground_row_compression -i ../../../sql/MsSql/gps_data/gps_data_row_compression.sql
time sqlcmd -S localhost -d playground_row_compression -i $SQL_FILE