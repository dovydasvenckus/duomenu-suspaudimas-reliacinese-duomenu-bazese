#!/usr/bin/env bash
SQL_FILE=${1?Give valid file name}
sqlcmd -S localhost -d playground_columnstore_archive -i ../../../sql/MsSql/gps_data/gps_data_columnstore_archive_compression.sql
time sqlcmd -S localhost -d playground_columnstore_archive -i $SQL_FILE