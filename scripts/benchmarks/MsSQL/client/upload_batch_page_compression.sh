#!/usr/bin/env bash
SQL_FILE=${1?Give valid file name}
sqlcmd -S localhost -d playground_page_compression -i ../../../sql/MsSql/client/client_page_compression.sql
time sqlcmd -S localhost -a 32767 -d playground_page_compression -i $SQL_FILE