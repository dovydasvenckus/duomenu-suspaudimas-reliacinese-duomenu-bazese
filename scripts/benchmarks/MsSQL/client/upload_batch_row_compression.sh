#!/usr/bin/env bash
SQL_FILE=${1?Give valid file name}
sqlcmd -S localhost -d playground_row_compression -i ../../../sql/MsSql/client/client_row_compression.sql
time sqlcmd -S localhost -a 32767 -d playground_row_compression -i $SQL_FILE