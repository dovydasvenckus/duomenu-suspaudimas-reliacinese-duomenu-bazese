#!/bin/sh
SQL_FILE=${1?Give valid file name}
OUT_FILE=${SQL_FILE#"../"}
sed '1 i\START TRANSACTION;\n' $SQL_FILE | sed  '$s/$/\n\nCOMMIT;/' > "transactional_$OUT_FILE"