#!/usr/bin/env bash
SQL_FILE=${1?Give valid file name}
psql -d playground_toast_compression -U test -f ../../../sql/PostgreSQL/text_long/text_long_compressed.sql -q
time psql -d playground_toast_compression -U test -f $SQL_FILE -q
