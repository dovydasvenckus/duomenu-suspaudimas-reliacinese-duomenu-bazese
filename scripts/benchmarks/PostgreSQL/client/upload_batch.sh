#!/usr/bin/env bash
SQL_FILE=${1?Give valid file name}
psql -d playground -U test -f ../../../sql/PostgreSQL/client/client.sql -q
time psql -d playground -U test -f $SQL_FILE -q
