#!/usr/bin/env bash
pushd .
cd ../../data-generator
./gradlew installDist
cp -r build/install/data-generator ~1/
