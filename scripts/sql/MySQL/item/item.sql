DROP TABLE IF EXISTS item;

CREATE TABLE item(
  id              BIGINT PRIMARY KEY AUTO_INCREMENT,
  name            VARCHAR(100),
  color           VARCHAR(20),
  date_added      DATETIME,
  price           DECIMAL(18, 5),
  weight          FLOAT,
  number_in_stock INTEGER
);