DROP TABLE IF EXISTS gps_data;

CREATE TABLE gps_data(
  id              BIGINT PRIMARY KEY AUTO_INCREMENT,
  latitude        DOUBLE,
  longitude       DOUBLE,
  count           INT,
  receive_time    DATETIME
)ROW_FORMAT=COMPRESSED;