DROP TABLE IF EXISTS article;

CREATE TABLE article(
  id           BIGINT PRIMARY KEY AUTO_INCREMENT,
  first_name   VARCHAR(255),
  last_name    VARCHAR(255),
  title        VARCHAR(255),
  body         TEXT
);