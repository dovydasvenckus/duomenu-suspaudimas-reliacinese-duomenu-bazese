DROP TABLE IF EXISTS text_long;

CREATE TABLE text_long(
  id           BIGINT IDENTITY(1, 1) PRIMARY KEY,
  text_field   TEXT
);