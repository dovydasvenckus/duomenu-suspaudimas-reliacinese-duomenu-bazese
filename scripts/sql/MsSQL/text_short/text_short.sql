DROP TABLE IF EXISTS text_short;

CREATE TABLE text_short(
  id           BIGINT IDENTITY(1, 1) PRIMARY KEY,
  text_field   VARCHAR(100)
);