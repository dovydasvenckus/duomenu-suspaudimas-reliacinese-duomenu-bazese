DROP TABLE IF EXISTS article;

CREATE TABLE article(
  id           BIGINT IDENTITY(1, 1) PRIMARY KEY,
  first_name   VARCHAR(255),
  last_name    VARCHAR(255),
  title        VARCHAR(255),
  body         TEXT
) WITH (DATA_COMPRESSION = PAGE);