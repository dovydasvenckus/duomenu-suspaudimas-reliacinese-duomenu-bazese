DROP TABLE IF EXISTS item;

CREATE TABLE item(
  id              BIGINT IDENTITY(1, 1) PRIMARY KEY,
  name            VARCHAR(100),
  color           VARCHAR(20),
  date_added      DATETIME,
  price           DECIMAL(18, 5),
  weight          REAL,
  number_in_stock INTEGER
) WITH (DATA_COMPRESSION = ROW);