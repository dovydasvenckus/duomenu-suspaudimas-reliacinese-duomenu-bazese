DROP TABLE IF EXISTS item;

CREATE TABLE item(
  id              BIGINT IDENTITY(1, 1),
  name            VARCHAR(100),
  color           VARCHAR(20),
  date_added      DATETIME,
  price           DECIMAL(18, 5),
  weight          REAL,
  number_in_stock INTEGER
);

CREATE CLUSTERED COLUMNSTORE INDEX item_cidx ON item WITH (DATA_COMPRESSION = COLUMNSTORE_ARCHIVE);