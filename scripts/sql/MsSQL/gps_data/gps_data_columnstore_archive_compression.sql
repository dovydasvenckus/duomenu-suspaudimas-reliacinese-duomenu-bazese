DROP TABLE IF EXISTS gps_data;

CREATE TABLE gps_data(
  id              BIGINT IDENTITY(1, 1),
  latitude        FLOAT(53),
  longitude       FLOAT(53),
  count           INT,
  receive_time    DATETIME
);

CREATE CLUSTERED COLUMNSTORE INDEX gps_data_cidx ON gps_data WITH (DATA_COMPRESSION = COLUMNSTORE_ARCHIVE);