DROP TABLE IF EXISTS gps_data;

CREATE TABLE gps_data(
  id              BIGINT IDENTITY(1, 1) PRIMARY KEY,
  latitude        FLOAT(53),
  longitude       FLOAT(53),
  count           INT,
  receive_time    DATETIME
);