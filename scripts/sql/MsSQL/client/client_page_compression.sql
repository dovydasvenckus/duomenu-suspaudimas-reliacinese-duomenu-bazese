DROP TABLE IF EXISTS client;

CREATE TABLE client(
  id           BIGINT IDENTITY(1, 1) PRIMARY KEY,
  username     VARCHAR(255),
  first_name   VARCHAR(127),
  last_name    VARCHAR(127),
  personal_id  VARCHAR(50),
  city         VARCHAR(255),
  address      VARCHAR(255),
  phone        VARCHAR(20)
)
WITH (DATA_COMPRESSION = PAGE);