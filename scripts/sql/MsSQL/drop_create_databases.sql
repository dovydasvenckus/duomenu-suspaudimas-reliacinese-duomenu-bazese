DROP DATABASE IF EXISTS playground;
DROP DATABASE IF EXISTS playground_columnstore;
DROP DATABASE IF EXISTS playground_columnstore_archive;
DROP DATABASE IF EXISTS playground_page_compression;
DROP DATABASE IF EXISTS playground_row_compression;

CREATE DATABASE playground;
CREATE DATABASE playground_columnstore;
CREATE DATABASE playground_columnstore_archive;
CREATE DATABASE playground_page_compression;
CREATE DATABASE playground_row_compression;