SELECT table_name,
    table_bytes / 1024 AS TABLE_KB,
    index_bytes / 1024 AS INDEX_KB,
    toast_bytes / 1024 AS TOAST_KB,
    total_bytes / 1024 AS TOTAL_KB,
    (table_bytes + toast_bytes) / 1024 AS TABLE_WITH_TOAST_KB
  FROM (
  SELECT *,
      total_bytes-index_bytes-COALESCE(toast_bytes,0)
      AS table_bytes FROM (
      SELECT c.oid,nspname AS table_schema, relname AS TABLE_NAME,
          pg_total_relation_size(c.oid) AS total_bytes,
          pg_indexes_size(c.oid) AS index_bytes,
          pg_total_relation_size(reltoastrelid) AS toast_bytes
          FROM pg_class c
          LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
          WHERE relkind = 'r' AND nspname='public'
  ) a
) a;
