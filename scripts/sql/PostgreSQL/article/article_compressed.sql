DROP TABLE IF EXISTS article;

CREATE TABLE article(
  id           BIGSERIAl PRIMARY KEY,
  first_name   VARCHAR(255),
  last_name    VARCHAR(255),
  title        VARCHAR(255),
  body         TEXT
);