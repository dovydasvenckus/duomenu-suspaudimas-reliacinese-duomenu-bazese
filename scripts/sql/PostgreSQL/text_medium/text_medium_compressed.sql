DROP TABLE IF EXISTS text_medium;

CREATE TABLE text_medium(
  id           BIGSERIAl PRIMARY KEY,
  text_field   TEXT
);