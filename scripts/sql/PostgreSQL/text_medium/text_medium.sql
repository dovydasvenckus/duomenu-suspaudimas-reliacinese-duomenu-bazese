DROP TABLE IF EXISTS text_medium;

CREATE TABLE text_medium(
  id           BIGSERIAl PRIMARY KEY,
  text_field   TEXT
);

ALTER TABLE text_medium ALTER COLUMN text_field SET STORAGE EXTERNAL;