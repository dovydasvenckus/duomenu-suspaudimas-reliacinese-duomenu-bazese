DROP TABLE IF EXISTS gps_data;

CREATE TABLE gps_data(
  id              BIGSERIAl PRIMARY KEY,
  latitude        DOUBLE PRECISION,
  longitude       DOUBLE PRECISION,
  count           INT,
  receive_time    TIMESTAMP
);