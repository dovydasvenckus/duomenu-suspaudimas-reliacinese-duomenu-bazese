DROP TABLE IF EXISTS text_long;

CREATE TABLE text_long(
  id           BIGSERIAl PRIMARY KEY,
  text_field   TEXT
);