DROP TABLE IF EXISTS text_long;

CREATE TABLE text_long(
  id           BIGSERIAl PRIMARY KEY,
  text_field   TEXT
);

ALTER TABLE text_long ALTER COLUMN text_field SET STORAGE EXTERNAL;