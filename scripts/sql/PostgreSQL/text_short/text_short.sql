DROP TABLE IF EXISTS text_short;

CREATE TABLE text_short(
  id           BIGSERIAl PRIMARY KEY,
  text_field   TEXT
);

ALTER TABLE text_short ALTER COLUMN text_field SET STORAGE EXTERNAL;