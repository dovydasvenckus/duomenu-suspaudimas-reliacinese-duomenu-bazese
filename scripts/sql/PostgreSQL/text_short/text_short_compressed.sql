DROP TABLE IF EXISTS text_short;

CREATE TABLE text_short(
  id           BIGSERIAl PRIMARY KEY,
  text_field   TEXT
);