CREATE TABLE item(
  id              BIGINT PRIMARY KEY,
  name            VARCHAR(100),
  color           VARCHAR(20),
  date_added      TIMESTAMP,
  price           DECIMAL(18, 5),
  weight          FLOAT,
  number_in_stock INTEGER
);