CREATE TABLE gps_data(
  id              BIGINT PRIMARY KEY,
  latitude        DOUBLE PRECISION,
  longitude       DOUBLE PRECISION,
  count           INTEGER,
  receive_time    TIMESTAMP
);