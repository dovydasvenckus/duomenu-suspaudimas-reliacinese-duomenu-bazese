\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{graph}
\LoadClass[12pt, a4paper]{standalone}

\RequirePackage{fontspec}
\RequirePackage{polyglossia}
\RequirePackage{tikz}
\RequirePackage{pgfplots}

\setmainfont{Palemonas}

\pgfplotsset{compat=1.13}
