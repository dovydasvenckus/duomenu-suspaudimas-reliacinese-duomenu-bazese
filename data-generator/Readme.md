# Readme

## Installation
To download dependencies and build binary executable run this command:

     ./gradlew installDist

## Usage Examples
To run application execute this command:
    
    sh build/install/data-generator/bin/data-generator -c 100 CLIENT

To list available params: 

    sh build/install/data-generator/bin/data-generator