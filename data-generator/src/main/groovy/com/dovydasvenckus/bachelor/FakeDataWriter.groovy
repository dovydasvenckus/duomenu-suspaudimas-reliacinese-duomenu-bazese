package com.dovydasvenckus.bachelor

import com.dovydasvenckus.bachelor.fake.FakeData
import groovy.transform.CompileStatic

import java.util.function.Consumer
import java.util.function.Supplier

@CompileStatic
class FakeDataWriter {


    static void writeDataToFile(FakeData fakeData, Long entityCount) {
        FileWriter fileWriter = new FileWriter(createFile(fakeData), true)
        BufferedWriter buffWriter = new BufferedWriter(fileWriter, 1024 * 64)

        Supplier supplier = fakeData.getSupplier()
        Consumer consumer = fakeData.getConsumer(buffWriter)

        (1..entityCount).parallelStream().forEach({
            consumer.accept(supplier.get())
        })

        buffWriter.flush()
    }

    private static File createFile(FakeData fakeData) {
        String fileName = getFileName(fakeData)
        File output = new File(fileName)
        output.delete()
        return new File(fileName)
    }

    private static String getFileName(FakeData fakeData) {
        return "${fakeData.toString().toLowerCase()}.sql"
    }
}
