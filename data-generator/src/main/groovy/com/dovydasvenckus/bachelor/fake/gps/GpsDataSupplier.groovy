package com.dovydasvenckus.bachelor.fake.gps

import com.github.javafaker.Address
import com.github.javafaker.DateAndTime
import com.github.javafaker.Faker
import com.github.javafaker.Number
import groovy.transform.CompileStatic

import java.util.function.Supplier

import static java.util.concurrent.TimeUnit.DAYS

@CompileStatic
class GpsDataSupplier implements Supplier<GpsData> {
    private final int maxDaysOffset = 365 * 5

    private final Faker faker = new Faker()
    private final Address address = faker.address()
    private final DateAndTime dateAndTime = faker.date()
    private final Number number = faker.number()


    @Override
    GpsData get() {
        return new GpsData(
                latitude: Double.parseDouble(address.latitude()),
                longitude: Double.parseDouble(address.longitude()),
                count: number.numberBetween(0, 100000),
                receiveTime: dateAndTime.past(maxDaysOffset, DAYS)
        )
    }
}
