package com.dovydasvenckus.bachelor.fake.article

import com.dovydasvenckus.bachelor.fake.WriteToFileConsumer
import groovy.transform.CompileStatic

import static com.dovydasvenckus.bachelor.util.SqlStringHelper.escapeString

@CompileStatic
class ArticleConsumer extends WriteToFileConsumer<Article> {

    ArticleConsumer(Writer writer) {
        super(writer)
    }

    @Override
    String createSqlString(Article article) {
        return "INSERT INTO article (first_name, last_name, title, body) " +
                "VALUES (${escapeString(article.firstName)}, ${escapeString(article.lastName)}," +
                " ${escapeString(article.title)}, ${escapeString(article.body)});\n"
    }
}
