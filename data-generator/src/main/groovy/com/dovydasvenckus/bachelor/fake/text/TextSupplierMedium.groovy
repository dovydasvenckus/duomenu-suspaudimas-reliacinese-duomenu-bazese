package com.dovydasvenckus.bachelor.fake.text

import groovy.transform.CompileStatic

@CompileStatic
class TextSupplierMedium extends TextSupplier {

    @Override
    int getNumberOfCharacters() {
        return 1000
    }
}
