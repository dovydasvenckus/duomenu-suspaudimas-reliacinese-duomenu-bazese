package com.dovydasvenckus.bachelor.fake.gps

import groovy.transform.CompileStatic

@CompileStatic
class GpsData {
    Double latitude
    Double longitude
    Integer count
    Date receiveTime
}
