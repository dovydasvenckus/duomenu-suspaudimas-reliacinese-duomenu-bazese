package com.dovydasvenckus.bachelor.fake.text

import com.dovydasvenckus.bachelor.fake.WriteToFileConsumer
import groovy.transform.CompileStatic

import static com.dovydasvenckus.bachelor.util.SqlStringHelper.escapeString

@CompileStatic
abstract class TextConsumer extends WriteToFileConsumer<Text> {

    TextConsumer(Writer writer) {
        super(writer)
    }

    @Override
    String createSqlString(Text text) {
        return "INSERT INTO ${tableName()} (text_field) VALUES (${escapeString(text.text)});\n"
    }
    
    abstract String tableName()
}
