package com.dovydasvenckus.bachelor.fake.text

import groovy.transform.CompileStatic

@CompileStatic
class TextConsumerMedium extends TextConsumer{
    TextConsumerMedium(Writer writer) {
        super(writer)
    }

    @Override
    String tableName() {
        return 'text_medium'
    }
}
