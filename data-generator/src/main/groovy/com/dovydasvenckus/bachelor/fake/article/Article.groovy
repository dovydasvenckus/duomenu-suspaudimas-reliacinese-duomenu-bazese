package com.dovydasvenckus.bachelor.fake.article

import groovy.transform.CompileStatic

@CompileStatic
class Article {
    String firstName
    String lastName
    String title
    String body
}
