package com.dovydasvenckus.bachelor.fake.gps

import com.dovydasvenckus.bachelor.fake.WriteToFileConsumer

import static com.dovydasvenckus.bachelor.util.SqlStringHelper.dateTimeToString
import static com.dovydasvenckus.bachelor.util.SqlStringHelper.escapeString

class GpsDataConsumer extends WriteToFileConsumer<GpsData> {

    GpsDataConsumer(Writer writer) {
        super(writer)
    }

    @Override
    String createSqlString(GpsData gpsData) {
        return "INSERT INTO gps_data (latitude, longitude, count, receive_time) " +
                "VALUES (${gpsData.latitude}, ${gpsData.longitude}, " +
                "${gpsData.count}, ${dateTimeToString(gpsData.receiveTime)});\n"
    }
}
