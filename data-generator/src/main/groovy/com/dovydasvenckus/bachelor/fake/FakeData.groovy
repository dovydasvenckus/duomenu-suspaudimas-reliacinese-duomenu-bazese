package com.dovydasvenckus.bachelor.fake

import com.dovydasvenckus.bachelor.fake.article.ArticleConsumer
import com.dovydasvenckus.bachelor.fake.article.ArticleSupplier
import com.dovydasvenckus.bachelor.fake.client.ClientConsumer
import com.dovydasvenckus.bachelor.fake.client.ClientSupplier
import com.dovydasvenckus.bachelor.fake.gps.GpsDataConsumer
import com.dovydasvenckus.bachelor.fake.gps.GpsDataSupplier
import com.dovydasvenckus.bachelor.fake.item.ItemConsumer
import com.dovydasvenckus.bachelor.fake.item.ItemSupplier
import com.dovydasvenckus.bachelor.fake.text.*
import groovy.transform.CompileStatic

import java.util.function.Consumer
import java.util.function.Supplier

@CompileStatic
enum FakeData {
    CLIENT, ARTICLE, GPS_DATA, ITEM, TEXT_SHORT, TEXT_MEDIUM, TEXT_LONG

    Supplier getSupplier() {
        switch (this) {
            case CLIENT:
                return new ClientSupplier()
            case ARTICLE:
                return new ArticleSupplier()
            case GPS_DATA:
                return new GpsDataSupplier()
            case ITEM:
                return new ItemSupplier()
            case TEXT_SHORT:
                return new TextSupplierShort()
            case TEXT_MEDIUM:
                return new TextSupplierMedium()
            case TEXT_LONG:
                return new TextSupplierLong()
        }

        return null
    }

    Consumer getConsumer(Writer writer) {
        switch (this) {
            case CLIENT:
                return new ClientConsumer(writer)
            case ARTICLE:
                return new ArticleConsumer(writer)
            case GPS_DATA:
                return new GpsDataConsumer(writer)
            case ITEM:
                return new ItemConsumer(writer)
            case TEXT_SHORT:
                return new TextConsumerShort(writer)
            case TEXT_MEDIUM:
                return new TextConsumerMedium(writer)
            case TEXT_LONG:
                return new TextConsumerLong(writer)
        }

        return null
    }
}