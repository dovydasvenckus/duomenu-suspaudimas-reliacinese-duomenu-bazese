package com.dovydasvenckus.bachelor.fake.text

import com.github.javafaker.Faker
import com.github.javafaker.Lorem
import groovy.transform.CompileStatic

import java.util.function.Supplier

@CompileStatic
abstract class TextSupplier implements Supplier<Text> {

    private Faker faker = new Faker()
    private Lorem lorem = faker.lorem()
    
    @Override
    Text get() {
        return new Text(text: lorem.fixedString(getNumberOfCharacters()))
    }

    abstract int getNumberOfCharacters()
}
