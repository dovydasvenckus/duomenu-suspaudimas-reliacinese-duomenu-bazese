package com.dovydasvenckus.bachelor.fake.text

import groovy.transform.CompileStatic

@CompileStatic
class TextSupplierShort extends TextSupplier {

    @Override
    int getNumberOfCharacters() {
        return 100
    }
}
