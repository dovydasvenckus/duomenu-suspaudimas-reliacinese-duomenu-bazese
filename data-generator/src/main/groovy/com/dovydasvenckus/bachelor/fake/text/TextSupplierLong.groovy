package com.dovydasvenckus.bachelor.fake.text

import groovy.transform.CompileStatic

@CompileStatic
class TextSupplierLong extends TextSupplier {

    @Override
    int getNumberOfCharacters() {
        return 10000
    }
}
