package com.dovydasvenckus.bachelor.fake.item

import com.github.javafaker.Commerce
import com.github.javafaker.Faker
import com.github.javafaker.Number
import groovy.transform.CompileStatic

import java.math.RoundingMode
import java.util.concurrent.TimeUnit
import java.util.function.Supplier

@CompileStatic
class ItemSupplier implements Supplier<Item> {

    private final Integer maxPastDays = 365 * 5

    private final Faker faker = new Faker()
    private final Commerce commerce = faker.commerce()
    private final Number number = faker.number()


    @Override
    Item get() {
        return new Item(name: commerce.productName(),
                        color: commerce.color(),
                        dateCreated: faker.date().past(maxPastDays, TimeUnit.DAYS),
                        price: new BigDecimal(number.randomDouble(3, 1, 1000)).setScale(3, RoundingMode.HALF_UP),
                        numberInStock: number.numberBetween(0, 10000),
                        weight: number.randomDouble(3, 0, 500)
        )
    }
}
