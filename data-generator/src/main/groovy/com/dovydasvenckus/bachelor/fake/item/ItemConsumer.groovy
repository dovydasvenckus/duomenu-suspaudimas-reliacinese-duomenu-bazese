package com.dovydasvenckus.bachelor.fake.item

import com.dovydasvenckus.bachelor.fake.WriteToFileConsumer
import com.dovydasvenckus.bachelor.util.SqlStringHelper

import static com.dovydasvenckus.bachelor.util.SqlStringHelper.escapeString

class ItemConsumer extends WriteToFileConsumer<Item> {
    
    ItemConsumer(Writer writer) {
        super(writer)
    }
    
    @Override
    String createSqlString(Item item) {
        return "INSERT INTO item (name, color, date_added, price, weight, number_in_stock) " +
                "VALUES (${escapeString(item.name)}, ${escapeString(item.color)}, ${SqlStringHelper.dateTimeToString(item.dateCreated)}, " +
                "${item.price}, ${item.weight}, ${item.numberInStock});\n"
    }
    
    
}
