package com.dovydasvenckus.bachelor.fake.text

import groovy.transform.CompileStatic

@CompileStatic
class TextConsumerLong extends TextConsumer {
    TextConsumerLong(Writer writer) {
        super(writer)
    }

    @Override
    String tableName() {
        return 'text_long'
    }
}
