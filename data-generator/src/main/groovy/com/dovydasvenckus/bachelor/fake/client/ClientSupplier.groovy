package com.dovydasvenckus.bachelor.fake.client

import com.github.javafaker.Address
import com.github.javafaker.Faker
import com.github.javafaker.Name
import groovy.transform.CompileStatic

import java.util.function.Supplier

@CompileStatic
class ClientSupplier implements Supplier<Client> {

    private Faker faker = new Faker()

    @Override
    Client get() {
        return createFakeClient()
    }

    private Client createFakeClient() {
        Name name = faker.name()
        Address address = faker.address()

        Client client = new Client(name.firstName(), name.lastName())
        client.personalId = UUID.randomUUID().toString()

        client.city = address.city()
        client.address = address.streetAddress()
        client.phoneNumber = faker.phoneNumber().cellPhone()

        return client
    }
}
