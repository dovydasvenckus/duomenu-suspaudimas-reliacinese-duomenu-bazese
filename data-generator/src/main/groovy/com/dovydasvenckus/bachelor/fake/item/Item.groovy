package com.dovydasvenckus.bachelor.fake.item

import groovy.transform.CompileStatic

@CompileStatic
class Item {
    String name
    String color
    Date dateCreated
    BigDecimal price
    Integer numberInStock
    Double weight
}
