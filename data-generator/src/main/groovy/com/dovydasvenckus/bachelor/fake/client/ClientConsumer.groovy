package com.dovydasvenckus.bachelor.fake.client

import com.dovydasvenckus.bachelor.fake.WriteToFileConsumer
import groovy.transform.CompileStatic

import static com.dovydasvenckus.bachelor.util.SqlStringHelper.escapeString

@CompileStatic
class ClientConsumer extends WriteToFileConsumer<Client> {

    ClientConsumer(Writer writer) {
        super(writer)
    }

    @Override
    String createSqlString(Client client) {
        return "INSERT INTO client (username, first_name, last_name, personal_id, city, address, phone) " +
                "VALUES (${escapeString(client.username)}, " +
                "${escapeString(client.firstName)}, ${escapeString(client.lastName)}," +
                "${escapeString(client.personalId)}, ${escapeString(client.city)}, " +
                "${escapeString(client.address)}, ${escapeString(client.phoneNumber)});\n"
    }
}
