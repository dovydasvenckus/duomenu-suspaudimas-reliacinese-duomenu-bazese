package com.dovydasvenckus.bachelor.fake.text

import groovy.transform.CompileStatic

@CompileStatic
class TextConsumerShort extends TextConsumer{
    TextConsumerShort(Writer writer) {
        super(writer)
    }

    @Override
    String tableName() {
        return 'text_short'
    }
}
