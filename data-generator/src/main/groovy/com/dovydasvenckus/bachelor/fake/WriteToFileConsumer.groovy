package com.dovydasvenckus.bachelor.fake

import groovy.transform.CompileStatic

import java.util.function.Consumer

@CompileStatic
abstract class WriteToFileConsumer<T extends Object> implements Consumer<T> {
    Writer writer

    WriteToFileConsumer(Writer writer) {
        this.writer = writer
    }

    @Override
    void accept(T t) {
        writer << createSqlString(t)
    }

    abstract String createSqlString(T t)
}
