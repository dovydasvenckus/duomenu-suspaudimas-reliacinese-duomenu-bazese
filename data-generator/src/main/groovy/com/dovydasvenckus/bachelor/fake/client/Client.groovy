package com.dovydasvenckus.bachelor.fake.client

import groovy.transform.CompileStatic

@CompileStatic
class Client {
    String firstName
    String lastName
    String personalId
    String city
    String address
    String phoneNumber

    private Client() {}

    Client(String firstName, String lastName) {
        this.firstName = firstName
        this.lastName = lastName
    }

    String getUsername() {
        return (firstName.toLowerCase() + '.' + lastName.toLowerCase()).replace("'", "")
    }

}
