package com.dovydasvenckus.bachelor.fake.article

import com.github.javafaker.Faker
import com.github.javafaker.Lorem
import com.github.javafaker.Name
import groovy.transform.CompileStatic

import java.util.function.Supplier

@CompileStatic
class ArticleSupplier implements Supplier<Article> {

    private int minTitleLength = 3
    private int maxTitleLength = 10

    private int minBodyLength = 200
    private int maxBodyLength = 4000

    private Faker faker = new Faker()
    private Random random = new Random()

    @Override
    Article get() {
        Name name = faker.name()
        Lorem lorem = faker.lorem()
        return new Article(
                firstName: name.firstName(),
                lastName: name.lastName(),
                title: lorem.sentence(random.nextInt(maxTitleLength - minTitleLength) + minTitleLength),
                body:  lorem.sentence(random.nextInt(maxBodyLength - minBodyLength) + minBodyLength),
        )
    }
}
