package com.dovydasvenckus.bachelor

import com.dovydasvenckus.bachelor.fake.FakeData

class App {

    private final static Long DEFAULT_ENTITY_COUNT = 1000

    static void main(String[] args) {
        CliBuilder cliBuilder = initCliBuilder()
        def options = cliBuilder.parse(args)

        if (options.arguments()) {
            try {
                FakeData fakeDataType = FakeData.valueOf(options.arguments()[0].toUpperCase())
                Long entityCount =  options.c ? Long.parseLong(options.c) : DEFAULT_ENTITY_COUNT

                Date startDate = new Date()

                FakeDataWriter.writeDataToFile(fakeDataType, entityCount)

                Date endDate = new Date()

                println "Created ${entityCount} rows of ${fakeDataType.toString()}. It took ${endDate.time - startDate.time} miliseconds"
            }
            catch (NumberFormatException ex) {
                println "Count parameter must be integer"
            }
            catch (IllegalArgumentException ex) {
                println "Wrong data parameter. Available values ${FakeData.values().toString()}"
            }
        }

        else cliBuilder.usage()
    }

    private static CliBuilder initCliBuilder() {
        CliBuilder cliBuilder = new CliBuilder(usage: "data-generator [options] <data>", header:'Options:')
        cliBuilder._(longOpt: 'data', args: 1, argName: 'data', "Fake data set ${FakeData.values().toString()}")
        cliBuilder.c( args: 1, argName: 'count', 'Count of fake entries')
        cliBuilder.help('Display help menu')

        return cliBuilder
    }

}