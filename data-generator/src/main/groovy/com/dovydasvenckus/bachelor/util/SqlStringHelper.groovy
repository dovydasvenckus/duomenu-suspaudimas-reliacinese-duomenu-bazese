package com.dovydasvenckus.bachelor.util

import groovy.transform.CompileStatic

@CompileStatic
class SqlStringHelper {

    static String escapeString(String string) {
        String escapedString = string.replace("'", "''")
        return "'" + escapedString + "'"
    }

    static String dateTimeToString(Date date) {
        return "'${date.format("yyyy-MM-dd HH:mm:ss.SSS")}'"
    }

}
